import st3m.run
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputState
import random
import leds
import captouch
import sys_buttons
import bl00mbox
import json

class PetalChallenge():
    def __init__(self):
        self.active = False
        self.time_left = 0

class Configuration:
    def __init__(self) -> None:
        self.highscore = 0
        self.brightness: float = 0.6
        self.times_played: int = 0
        self.font: str = "Camp Font 2"

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "highscore" in data and type(data["highscore"]) == int:
            res.highscore = data["highscore"]
        if "brightness" in data and type(data["brightness"]) == float:
            res.brightness = data["brightness"]
        if "times_played" in data and type(data["times_played"]) == int:
            res.times_played = data["times_played"]
        if "font" in data and type(data["font"]) == str:
            res.font = data["font"]
        return res

    def save(self, path: str) -> None:
        d = {
            "highscore": self.highscore,
            "brightness": self.brightness,
            "times_played": self.times_played,
            "font": self.font,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()   

class ReactionGame(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.cap_state = captouch.read()
        _path = app_ctx.bundle_path or "/flash/sys/apps/senor-reactions"
        self._filename = _path + "/config.json"
        self.TEXT_COLOUR_RUNNING = (0, 1, 0)
        self.TEXT_COLOUR_FINISHED = (1, 0, 0)
        self.TEXT_COLOUR_WAITING = (0, 0, 1)
        self.FONT_SIZE = 25
        self.BRIGHTNESS_DELTA = 0.01
        self.last_button_press = 1500
        self.score = 0
        self.time_till_challenge = 0
        self.time_till_challenge_default = 1200
        self.time_for_challenge = 1500
        self.game_state = "PRE_LOADING"
        self.petal_data = {}
        for i in range(10):
            self.petal_data[i] = PetalChallenge()
        self._config = Configuration.load(self._filename)
        # load audio
        self.blm = bl00mbox.Channel("reactions")
        self.blm.volume = 5000

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = self._config.font
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        
        if self.game_state == "PRE_LOADING":
            #draw leds
            leds.set_all_rgb(0,0,1)
            
            # draw text
            ctx.move_to(0,0)
            ctx.font_size = self.FONT_SIZE * 3
            ctx.rgb(*self.TEXT_COLOUR_WAITING)
            ctx.text("LOADING")
            self.game_state = "LOADING"

        if self.game_state == "WAITING":
            #draw leds
            leds.set_brightness(int(self._config.brightness * 255))
            leds.set_all_rgb(0, 0, 1)
            #draw text 
            ctx.rgb(*self.TEXT_COLOUR_WAITING)
            ctx.move_to(0, -self.FONT_SIZE * 2.5)
            ctx.font_size = self.FONT_SIZE * 0.5
            ctx.text("Touch the lighting petals")            
            ctx.move_to(0, -self.FONT_SIZE * 2)
            ctx.text("before they turn red")
            ctx.move_to(0, -self.FONT_SIZE)
            ctx.font_size = self.FONT_SIZE
            ctx.text("Higscore:")
            ctx.move_to(0, self.FONT_SIZE * 1)
            ctx.font_size = self.FONT_SIZE * 1.5
            ctx.text(str(self._config.highscore))
            ctx.move_to(0, self.FONT_SIZE * 2.5)
            ctx.font_size = self.FONT_SIZE * 0.5
            ctx.text("Press left button to start")

        if self.game_state == "RUNNING":
            # draw leds
            leds.set_brightness(int(self._config.brightness * 255))
            leds.set_all_rgb(0, 0, 0)
            for petal_no in range(0,10):
                if self.petal_data[petal_no].active:
                    petal_progress =  self.petal_data[petal_no].time_left / self.time_for_challenge
                    leds.set_rgb((petal_no * 4 - 1) % 40, 1 - petal_progress, petal_progress, 0)
                    leds.set_rgb(petal_no * 4, 1 - petal_progress, petal_progress, 0)
                    leds.set_rgb((petal_no * 4 + 1) % 40, 1 - petal_progress, petal_progress, 0)
            # draw text
            ctx.rgb(*self.TEXT_COLOUR_RUNNING)
            ctx.move_to(0, -self.FONT_SIZE)
            ctx.font_size = self.FONT_SIZE
            ctx.text("Score:")
            ctx.move_to(0, self.FONT_SIZE * 1.5)
            ctx.font_size = self.FONT_SIZE * 1.5
            ctx.text(str(self.score))

        if self.game_state == "FINISHED":
            
            #draw leds
            leds.set_all_rgb(1, 0 , 0)

            #draw text
            ctx.rgb(*self.TEXT_COLOUR_FINISHED)
            ctx.move_to(0, -self.FONT_SIZE)
            ctx.font_size = self.FONT_SIZE
            ctx.text("Score:")
            ctx.move_to(0, self.FONT_SIZE * 1.5)
            ctx.font_size = self.FONT_SIZE * 1.5
            ctx.text(str(self.score))
            ctx.move_to(0, self.FONT_SIZE * 2.5)
            ctx.font_size = self.FONT_SIZE * 0.5
            ctx.text("Press left button to restart")
        leds.update()
        
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.cap_state = captouch.read()
        self.time_till_challenge -= delta_ms

        if sys_buttons.get_left() == sys_buttons.PRESSED_LEFT:
            self._config.brightness -= self.BRIGHTNESS_DELTA
        if sys_buttons.get_left() == sys_buttons.PRESSED_RIGHT:
            self._config.brightness += self.BRIGHTNESS_DELTA

        self._config.brightness = min(1, self._config.brightness)
        self._config.brightness = max(0.1, self._config.brightness)   

        if self.game_state == "LOADING":
            self.beep = self.blm.new(bl00mbox.patches.sampler,"/sys/apps/senor-reactions/samples/beep.wav")
            self.beep.signals.output = self.blm.mixer
            self.fail = self.blm.new(bl00mbox.patches.sampler,"/sys/apps/senor-reactions/samples/fail.wav")
            self.fail.signals.output = self.blm.mixer
            self.win = self.blm.new(bl00mbox.patches.sampler,"/sys/apps/senor-reactions/samples/win.wav")
            self.win.signals.output = self.blm.mixer
            self.game_state = "WAITING"

        if self.game_state == "RUNNING":
            all_active = True
            for petal_no in range(0, 10):
                if self.petal_data[petal_no].active:
                    self.petal_data[petal_no].time_left -= delta_ms
                    # check if petal is pressed
                    if self.cap_state.petals[petal_no].pressed:
                        self.petal_data[petal_no].time_left = 0
                        self.petal_data[petal_no].active = False
                        self.score += 1
                    if self.petal_data[petal_no].time_left < 0:
                        self.game_state = "FINISHED"
                        if self.score > self._config.highscore:
                            self._config.highscore = self.score
                            self.win.signals.trigger.start()
                        else:
                            self.fail.signals.trigger.start()
                else:
                    all_active = False
            if all_active:
                self.game_state = "FINISHED"
                if self.score > self._config.highscore:
                    self._config.highscore = self.score
                    self.win.signals.trigger.start()
                else:
                    self.fail.signals.trigger.start()

        if self.time_till_challenge <= 0 and self.game_state == "RUNNING":
            petal = None
            while not petal:
                petal = random.randint(0,9)
                if self.petal_data[petal].active:
                    petal = None

            self.petal_data[petal].active = True
            self.petal_data[petal].time_left = self.time_for_challenge
            self.time_till_challenge = self.time_till_challenge_default - self.score * 10
            self.beep.signals.trigger.start()

        if self.game_state == "WAITING" and sys_buttons.get_left() == sys_buttons.PRESSED_DOWN and self.last_button_press <= 0:
            self.last_button_press = 1500
            self.game_state = "RUNNING"

        if self.game_state == "FINISHED" and sys_buttons.get_left() == sys_buttons.PRESSED_DOWN and self.last_button_press <= 0:
            
            self.last_button_press = 1500
            self.score = 0
            self.time_till_challenge = 0
            self.game_state = "WAITING"
            self.petal_data = {}
            for i in range(10):
                self.petal_data[i] = PetalChallenge()

        if self.last_button_press >= 0 and sys_buttons.get_left() == sys_buttons.NOT_PRESSED:
            self.last_button_press -= delta_ms

    def on_exit(self) -> None:
        leds.set_all_rgb(0, 0, 0)
        leds.update()
        self._config.save(self._filename)

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(ReactionGame(ApplicationContext()))